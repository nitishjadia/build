# Build container images using GitLab CI/CD

Project to use Gitlab CI/CD to build and host container images.   


## Steps

Create a new branch from `master`.   

Add required files and `Dockerfile` to the branch.   

Use the [Docker.gitlab-ci.yml template](Docker.gitlab-ci.yml) to create a new
`.gitlab-ci.yml` file which will build the image.